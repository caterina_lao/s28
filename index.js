//3

fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((json) => console.log(json))

//4

	// let url = "https://jsonplaceholder.typicode.com/todos";
	// let data = fetch(url)
	// let mappedData = data.map(function(data){
	// 	console.log(data);
	// 	return(data)
	// });


 //5 & 6
 	async function fetchData () {
 		let result = await fetch ('https://jsonplaceholder.typicode.com/todos/12')
 	
 		console.log(result);

 		console.log(result.body);

 		console.log(result.status);

 		let json = await result.json()
 		console.log(json)
 	}
	fetchData();


 //7

  fetch('https://jsonplaceholder.typicode.com/todos', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify ({
				title: 'New Post',
				body: 'New Artist',
				userId: 101
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))

//8

   	fetch('https://jsonplaceholder.typicode.com/todos/1', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: 'Updated Post',
				body: 'Updated New Artist'
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))

//9
	fetch('https://jsonplaceholder.typicode.com/todos/2', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: 'Updated Post',
				description: 'Updated the post',
				status: null,
				dateCompleted: 'October 1, 1990',
				userId: 123
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))


//10

	fetch('https://jsonplaceholder.typicode.com/todos/2',{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				description: 'Corrected content'
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))

//11
	fetch('https://jsonplaceholder.typicode.com/todos',{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				status: 'Completed',
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))
